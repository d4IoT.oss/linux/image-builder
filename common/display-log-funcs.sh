#!/bin/false
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################



script_name=$( basename ${0#-} )
current_script=$( basename ${BASH_SOURCE} )
if [ -z "$CFG_DISPLAY_LOG_FUNCS" ] ; then
  export CFG_DISPLAY_LOG_FUNCS=1
  #Lets also set some message standards for parsing
  export G_ERROR_NOT_INSTALLED="Not-I-N-S-T-A-L-L-E-D"
  export G_ERROR_NOT_ENABLED="Not-E-N-A-B-L-E-D"
  export G_ERROR_BYPASSED="B-y-p-a-s-s-e-d"
else
  return 0
fi

export G_log_timestamp_fmt=""
function set_log_module_id() {
#########################################################
# @Description set the modul elane for echo
#########################################################
 G_func_depth_id="($*)"
}

function _display() {
#########################################################
# @Description Display message to screen
#########################################################
local dt=""
[ ! -z "$G_log_timestamp_fmt" ] && dt=`date "$G_log_timestamp_fmt" `
    echo -e "   $* $dt\e[0m "
}
export -f _display
function log_msg() {
#########################################################
# @Description Display message to screen
#########################################################
    _display '\e[40m\e[34m' "MSG $*"
}
export -f log_msg

export G_base_shell_level=$SHLVL
function log_seq() {
#########################################################
# @Description Display Sequence to screen and auto update sequence
#########################################################
    local level=-1
    let "level = $SHLVL - $G_base_shell_level"
    [ -z "$G_log_seq" ] && let G_log_seq=1
    _display '\e[40m\e[35m' "#${level}.$G_log_seq$G_func_depth_id $*"
    let G_log_seq++
}
export -f log_seq
function log_error() {
#########################################################
# @Description Display error message and auto count
#########################################################
    _display '\e[40m\e[31m' "ERROR $*"
}
export -f log_error
function log_error_exit() {
#########################################################
# @Description Display error message and auto count
#########################################################
    log_error $1
    local retCode=1
    [[ ! -z "$2" ]] && retCode=$2 || :
    exit $retCode
}
export -f log_error_exit
function log_warn() {
#########################################################
# @Description Display warning message and auto count
#########################################################
    _display '\e[40m\e[33m' "WARN $*"
}
export -f log_warn
function log_info() {
#########################################################
# @Description Display informational message and auto count
#########################################################
    _display '\e[40m\e[32m' "INFO $*"
}
export -f log_info

function log_usage_exit() {
#########################################################
# @Description Display informational message and auto count
#########################################################
  local retCode=0
  if [[ ! -z  "$2" ]]; then
    log_error "$2"
    retCode=1
  fi
  echo -e "\e[40m\e[34m$1\e[0m "
  exit $retCode

}
export -f log_usage_exit


function display_trim_spaces() {
#########################################################
# @Description Echo with Trimmed spaces
#########################################################
    local var="$@"
    var="${var#"${var%%[![:space:]]*}"}"   # remove leading whitespace
    var="${var%"${var##*[![:space:]]}"}"   # remove trailing whitespace
    echo $var
}
export -f display_trim_spaces
