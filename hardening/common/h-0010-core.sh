#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Harden basic elements
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done





my_dir="$(dirname "$0")"

log_seq "Disabling password for root"
passwd -l root >/dev/null 2>&1

#########################################################


tFile='/etc/login.defs'
log_seq "Changing different parameters of password aging in $tFile"
tNumVal=0
tNumNewVal=60
tNumVal=`grep "^PASS_MAX_DAYS" $tFile | awk 'NF>1{print $NF}'`
if [ $tNumVal -gt $tNumNewVal ]
then
    log_warn "Changing PASS_MAX_DAYS from $tNumVal to $tNumNewVal in $tFile"
    sed -i "/^PASS_MAX_DAYS/c\PASS_MAX_DAYS  $tNumNewVal" $tFile
fi
tNumVal=0
tNumVal=`grep "^PASS_MIN_DAYS" $tFile | awk 'NF>1{print $NF}'`
tNumNewVal=1
if [ $tNumVal -lt $tNumNewVal ]
then
    log_warn "Changing PASS_MIN_DAYS from $tNumVal to $tNumNewVal in $tFile"
    sed -i "/^PASS_MIN_DAYS/c\PASS_MIN_DAYS  $tNumNewVal" $tFile
fi
tNumVal=0
tNumVal=`grep "^PASS_MIN_LEN" $tFile | awk 'NF>1{print $NF}'`
tNumNewVal=16
if [[ ! -z "$tNumVal" &&  $tNumVal -lt $tNumNewVal ]]
then
    log_warn "Changing PASS_MIN_LEN from $tNumVal to $tNumNewVal in $tFile"
    sed -i "/^PASS_MIN_LEN/c\PASS_MIN_LEN   $tNumNewVal" $tFile
fi
tNumVal=0
tNumVal=`grep "^PASS_WARN_AGE" $tFile | awk 'NF>1{print $NF}'`
tNumNewVal=15
if [ $tNumVal -lt $tNumNewVal ]
then
    log_warn "Changing PASS_WARN_AGE from $tNumVal to $tNumNewVal in $tFile"
    sed -i "/^PASS_WARN_AGE/c\PASS_WARN_AGE   $tNumNewVal" $tFile
fi
tTmpVal=`grep "^ENCRYPT_METHOD" $tFile | awk 'NF>1{print $NF}'`
tNewTmpVal="SHA512"
if [ "$tTmpVal" != "$tNewTmpVal" ]
then
    log_warn "Changing ENCRYPT_METHOD from $tTmpVal to $tNewTmpVal in $tFile"
    sed -i "/^ENCRYPT_METHOD/c\ENCRYPT_METHOD   $tNewTmpVal" $tFile
fi


log_seq "Disable modules in modprobe"
disableModulesModprobe 'dccp'
disableModulesModprobe 'sctp'
disableModulesModprobe 'rds'
disableModulesModprobe 'tipc'
disableModulesModprobe 'bluetooth'
disableModulesModprobe 'net-pf-31'
disableModulesModprobe 'wireless'
disableModulesModprobe 'usb'
disableModulesModprobe 'cramfs'
disableModulesModprobe 'freevxfs'
disableModulesModprobe 'jffs2'
disableModulesModprobe 'hfs'
disableModulesModprobe 'hfsplus'
disableModulesModprobe 'squashfs'
disableModulesModprobe 'vfat'
disableModulesModprobe 'udf'





log_seq "Disable services"
disableService 'abrtd'
disableService 'avahi-daemon'
disableService 'atd'
disableService 'ntpdate'
disableService 'oddjobd'
disableService 'qpidd'
disableService 'rdisc'
disableService 'netconsole'
disableService 'bluetooth'
disableService 'usb-storage'
disableService 'iptables'
disableService 'ip6tables'

log_seq "Enable services"
###TODO: Enable when the rules are in
enableService 'firewalld'
enableService 'chronyd'
enableService 'crond'

#########################################################
log_seq "Copying banner and motd"
cp -f $my_dir/cfg/banner /etc/banner
cp -f $my_dir/cfg/motd /etc/motd



#########################################################
log_seq "Checking if directories are mounted on own partition"
checkMount "/tmp"
checkMount "/boot"
checkMount "/home"
checkMount "/var"
checkMount "/var/log"
checkMount "/var/log/audit"
checkMount "/var/tmp"
