#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
function usage {
  USAGE=$(cat <<-EOF
     @Usage
      $0 [-h]
    @Description Perform all hardening functionality
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See $0/common-funcs.sh
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

# lets leave set error to the calling script so they can decide exit of first
#   error of parse output for error
#set -e
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh


. $my_dir/common-funcs.sh

log_msg "$(date) Starting Common Hardening"
for f in $my_dir/h-*.sh
do
  #echo "file $f"
  fWE="${f%.*}"
  #echo "File $fWE"
  if [ -f "$fWE.template" ]
  then
    . "$fWE.template"
  fi
  if [ -f "$fWE.config" ]
  then
    . "$fWE.config"
  fi
  if [ -f "$fWE.funcs" ]
  then
    . "$fWE.funcs"
  fi
  . $f
done
# TEMP

log_msg "$(date) Done Common Hardening"
