#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Harden grub
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

log_seq "Processing GRUB"

tCurFile=/etc/grub2.cfg
if [ ! -L $tCurFile ]; then
  [ -f $tCurFile ] && log_error "file $tCurFile must be a link"
  [ -f /boot/grub/grub.cfg ] && ln -s /boot/grub/grub.cfg $tCurFile
  [ -f /boot/grub2/grub.cfg ] && ln -s /boot/grub/grub.cfg $tCurFile
fi
tCurFile=`readlink -f  /etc/grub2.cfg`

tDefFile=/etc/default/grub
t40File=/etc/grub.d/40_custom
t00File=/etc/grub.d/00_header
t10File=/etc/grub.d/10_linux

sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0 /g'  $t00File
sed -i 's/ --class os\"/ --class os --unrestricted\"/g' $t10File
if [ -f $tDefFile ]; then 
  sed -i 's/GRUB_TIMEOUT=.*/GRUB_TIMEOUT=0 /g'  $tDefFile
fi
if ! grep "^password_pbkdf2" $t40File >& /dev/null
then
  #Do wierd things for username and password
  #TODO: Just set a bad SHA value so it will never work. Test
  rsize=`shuf -i 78-98 -n 1`
  rpwd=`openssl rand $rsize | sha512sum`
  rpwd=${rpwd% *}
  rsize=`shuf -i 68-86 -n 1`
  ruser=`openssl rand -base64 $rsize`
  rsize=`shuf -i 8-16 -n 1`
  ruser=${ruser:1:$rsize}
  echo "set superusers=\"$ruser\"" >> $t40File
  echo "password_pbkdf2 $ruser grub.pbkdf2.sha512.10000.$rpwd" >> $t40File
fi

if ! grep "^password_pbkdf2" $tCurFile >& /dev/null
then
  #Do wierd things for username and password
  #TODO: Just set a bad SHA value so it will never work. Test
  rsize=`shuf -i 78-98 -n 1`
  rpwd=`openssl rand $rsize | sha512sum`
  rpwd=${rpwd% *}
  rsize=`shuf -i 68-86 -n 1`
  ruser=`openssl rand -base64 $rsize`
  rsize=`shuf -i 8-16 -n 1`
  ruser=${ruser:1:$rsize}
  echo "set superusers=\"$ruser\"" >> $tCurFile
  echo "password_pbkdf2 $ruser grub.pbkdf2.sha512.10000.$rpwd" >> $tCurFile
  sed -i 's/ --class os/ --class os --unrestricted/g'  $tCurFile
fi
