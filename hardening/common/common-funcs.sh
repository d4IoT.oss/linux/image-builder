#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Common functions
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done



#################### Function ###########################
# Get value of a name=value pair
#########################################################
function getPropertyValue() {
    local var=`echo "$@" | cut -d= -f2-`

    echo $(display_trim_spaces "$var")
}
#################### Function ###########################
# Get name of a name=value pair
#########################################################
function getPropertyName() {
    local var=`echo "$@" | cut -d= -f1`

    echo $(display_trim_spaces "$var")
}

#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
#########################################################################
#
#       Modifying functions
#
#########################################################################

#################### Function ###########################
# Update permsission if more restrictive than existing
#    $1 -> File
#    $2 -> Permission Octal String (without leading 0)
#########################################################
function setPermRestrictive() {
local tTmp1
local tTmp2
local tTmp3

    tTmp1=$((0`stat -c %a $1`))
    tTmp2=$(($tTmp1 & 077$2))

    if [ "$tTmp1" != "$tTmp2" ]
    then
        tTmp1=`printf "%o" $tTmp1`
        tTmp2=`printf "%o" $tTmp2`
        log_Warn "Chaning permissions for $1 from $tTmp1 to $tTmp2"
        chmod $tTmp2 $1
    fi


}

#################### Function ###########################
# Set the Owner and Group ownership and permissions
#        $1 -> File
#        $2 -> Owner Name
#        $3 -> Group Name
#        $4 -> Permission Octal String
#  If you specify "" for $2,$3 or $4 then respective
#       change will not be made
#########################################################
function setOGPerm() {
local tTmp1
local tTmp2
    if [ -n "$2" ]
    then
        tTmp1=`stat -c %U $1`
        if [ "$tTmp1" != "$2" ]
        then
            log_warn "Changing Ownership for file $1 from $tTmp1 to $2"
            chown $2 $1
        fi
    fi
    if [ -n "$3" ]
    then
        tTmp1=`stat -c %G $1`
        if [ "$tTmp1" != "$3" ]
        then
            log_warn "Changing Group for file $1 from $tTmp1 to $3"
            chgrp $3 $1
        fi
    fi
    if [ -n "$4" ]
    then
        tTmp1=`stat -c %a $1`
        if [ "$tTmp1" != "$4" ]
        then
            log_warn "Changing Permission for file $1 from $tTmp1 to $4"
            chmod $4 $1
        fi
    fi

}

#################### Function ###########################
# Insert or update as required to match specified string
#   $1 -> File
#   $2 -> Exact String
#   $3 -> Match Pattern
#   $4 -> Before
#   $5 -> After
# ex: abc.cfg "token=4" "token=[0-9]?" "^"
# Note: Update will be in-place. Insert will append.
#########################################################
function matchUpsert() {
local tmpk
local tmpv
    tmpk=`echo "$2" | sed -e 's/[]\/$*.^|[]/\\\&/g'`
    tmpv=`echo "$2" | sed -e 's/[\/&]/\\\&/g'`
    #echo $tmpk
    #echo $tmpv
    #echo $2
    #return
    if ! grep -E "$4$tmpk$5"  $1 >& /etc/null
    then
        log_warn "Setting $2 in $1"
        if grep -E  "$3"  $1 >& /etc/null
        then
            sed -i "s/$4$3$5/$tmpv/" $1
        else
            echo "$2" >> $1
        fi
    fi

}


#################### Function ###########################
# Insert or update property value as required
#   $1 -> File
#   $2 -> property
#   $3 -> value
#   $4 -> comment
# Note: Update will be in-place. Insert will append.
#########################################################
function matchUpsertProperty() {
    matchUpsert $1 "$4$2 = $3" "$2[ ]*=.*" '^.*' '$'

}


#################### Function ###########################
#
#########################################################
function serviceRestart() {
    if [ "`systemctl is-active $1`" != "active" ]
    then
        log_info "$1 wasnt running so attempting start"
        service $1 start  >& /dev/null
   else
        log_info "Attempting restart of $1"
        service $1 restart  >& /dev/null
   fi
   if [ "`systemctl is-active $1`" != "active" ]
   then
       log_error "$1 is `systemctl is-active $1`"
   else
       log_msg "$1 is `systemctl is-active $1`"
   fi

}
#################### Function ###########################
#
#########################################################
#################### Function ###########################
#
#########################################################
function disableService() {
local tmpVal
    tmpVal="$(systemctl is-enabled $1 2>/dev/null)"
    case "$tmpVal" in
        enabled) systemctl mask $1 2>/dev/null 1>&2
            service $1 stop >& /dev/null
            log_warn "Service $1 is `systemctl is-enabled $1` ( `systemctl is-active $1` )"
            ;;
		*)  log_info "Service $1 is  $tmpVal"
		    ;;
    esac

}


#################### Function ###########################
#
#########################################################
function enableService() {
local tmpVal
    tmpVal="$(systemctl is-enabled $1 2>/dev/null)"
    case "$tmpVal" in
        enabled) log_info "Service $1 is already enabled"
            ;;
        *) systemctl unmask $1 2>/dev/null 1>&2
           systemctl enable $1 2>/dev/null 1>&2
	   tmpVal="$(systemctl is-enabled $1 2>/dev/null)"
	   if [ "$tmpVal" == "enabled" ]; then
	      log_warn "Service $1 is enabled (`systemctl is-active $1` )"
	   else
	      log_error "Service $1 is $tmpVal (`systemctl is-active $1` )"
	   fi
	   ;;
    esac
    return 0

}
