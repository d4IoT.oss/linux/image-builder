#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description SELinux and Audit
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done


log_seq "Setting Up Audit"

((! type auditctl >/dev/null 2>&1 )   ||  (auditctl -s 2>&1 | grep "audit support not in kernel") ) && log_warn "No Audit service"; disableService 'auditd'; return 0

enableService 'auditd'

if  grep "\s*linux\s*/vmlinuz-linux " /etc/grub2.cfg | grep -v "audit=1"  >& /dev/null
then
  log_warn "grub2 menu updated with audit"
  sed -i '/^\s?linux\s\/vmzlinuz-linux\s?/ s/$/ audit=1/' /etc/grub2.cfg
fi

if [ -f /etc/default/grub.cfg ]; then
  if  ! grep "^GRUB_CMDLINE_LINUX_DEFAULT=.*audit=1" /etc/default/grub.cfg   >& /dev/null
  then
    log_warn "grub2 default updated with audit"
    sed -i '/^GRUB_CMDLINE_LINUX_DEFAULT="/GRUB_CMDLINE_LINUX_DEFAULT="audit=1 /g' /etc/default/grub.cfg
  fi
fi


tAudRulesFile="/etc/audit/rules.d/audit.rules"
[ -f /etc/audit/audit.rules ] && tAudRulesFile="/etc/audit/audit.rules"
for tNewVal in 'settimeofday' 'clock_settime' 'adjtimex'
do
    if ! auditctl -l | grep  -e " -S $tNewVal " >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -k audit_time_rules"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done
for tNewVal in 'setdomainname' 'sethostname'
do
    if ! auditctl -l | grep -e " -S $tNewVal " >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -k audit_network_changes"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done
for tNewVal in 'init_module' 'delete_module'
do
    if ! auditctl -l | grep -e " -S $tNewVal " >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -k audit_modules"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done

for tNewVal in 'chown' 'chmod' 'fchmod' 'fchmodat' 'fchown' 'fchownat' 'fremovexattr' 'fsetxattr' 'lchown' 'lremovexattr' 'lsetxattr' 'removexattr' 'setxattr'
do
    if ! auditctl -l | grep -e " -S $tNewVal " >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid>=500 -F auid!=4294967295 -k audit_perm_mod"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid=0  -k audit_perm_mod"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done
#EACCES=13 EPERM=1
for tNewVal in 'EACCES' 'EPERM'
do
    if ! auditctl -l | grep -e "exit=-$tNewVal" >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-$tNewVal -F auid>=500 -F auid!=4294967295 -k audit_access"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
        tTmp="-a always,exit -F arch=b64 -S creat -S open -S openat -S truncate -S ftruncate -F exit=-$tNewVal -F auid=0 -k audit_access"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done

for tNewVal in `find / -xdev -type f \( -perm -4000 -o -perm -2000 \) 2>/dev/null`
do
    if ! auditctl -l | grep -e "path=$tNewVal" >& /dev/null;
    then
        tTmp="-a always,exit -F path=$tNewVal -F perm=x -F auid>=500 -F auid!=4294967295 -k audit_privileged  "
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi

done


tNewVal="mount"
if ! auditctl -l | grep -e " -S $tNewVal " >& /dev/null;
then
    tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid>=500 -F auid!=4294967295 -k audit_export"
    echo $tTmp >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp"
    tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid=0 -k audit_export"
    echo $tTmp >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp"
fi

for tNewVal in 'unlink' 'unlinkat' 'rename' 'renameat'
do
    if ! auditctl -l | grep -e " -S $tNewVal " >& /dev/null;
    then
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid>=500 -F auid!=4294967295 -k audit_delete"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
        tTmp="-a always,exit -F arch=b64 -S $tNewVal -F auid=0 -k audit_delete"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done

## Start watches
tNewVal="/etc/localtime"
if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
then
    tTmp="-w $tNewVal -p wa  -k audit_time_rules"
    echo $tTmp >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp"
fi

tNewVal="/etc/selinux"
if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
then
    tTmp="-w $tNewVal -p wa  -k MAC-Policy"
    echo $tTmp >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp"
fi

for tNewVal in '/sbin/insmod' '/sbin/rmmod' '/sbin/modprobe'
do
    if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
    then
        tTmp="-w $tNewVal -p wa  -k audit_modules"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done

for tNewVal in '/etc/passwd' '/etc/shadow' '/etc/group' '/etc/gshadow' '/etc/security/opasswd'
do
    if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
    then
        tTmp="-w $tNewVal -p wa  -k audit_account_changes"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done
for tNewVal in '/etc/issue' '/etc/issue.net' '/etc/hosts' '/etc/sysconfig/network'
do
    if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
    then
        tTmp="-w $tNewVal -p wa  -k audit_network_changes"
        echo $tTmp >> $tAudRulesFile
        log_warn "Audit rules updated : $tTmp"
    fi
done

tNewVal="/etc/sudoers"
if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
then
    tTmp="-w $tNewVal -p wa  -k audit_sudoers"
    echo $tTmp >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp"
fi

tNewVal="/etc/sudoers.d"
if ! auditctl -l | grep -e "-w $tNewVal" >& /dev/null;
then
    tTmp="-w $tNewVal -p wa  -k audit_sudoers"
    echo "$tTmp" >> $tAudRulesFile
    log_warn "Audit rules updated : $tTmp $tAudRulesFile"
fi

serviceRestart auditd

tSEConfigFile=/etc/selinux/config
[ ! -f  $tSEConfigFile ] && log_warn "SE Linux not Available" ; exit 0

#########################################################

log_seq "Checking SElinux setting"
tRetVal=`sestatus | grep "Current mode" |awk 'NF>1{print $NF}'`
if [ $tRetVal != "enforcing" ]
then
    log_warn "Changing SELinux from $tRetVal to Enforcing"
    sed -i 's/^SELINUX=.*/SELINUX=enforcing/' $tSEConfigFile
fi
tRetVal=`sestatus | grep "Mode from config file" |awk 'NF>1{print $NF}'`
if [ $tRetVal != "enforcing" ]
then
    log_warn "Changing SELinux(config) from $tRetVal to Enforcing"
    sed -i 's/^SELINUX=.*/SELINUX=enforcing/' $tSEConfigFile
fi
touch /.autorelabel

#########################################################
