!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Harden sshd
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done



SCRIPTDIR="$(dirname "$(readlink -f "$0")")"

[ ! -f /etc/ssh/sshd_config.orig ] &&  cp /etc/ssh/sshd_config /etc/ssh/sshd_config.orig; cp $SCRIPTDIR/cfg/sshd_config /etc/ssh/sshd_config

log_seq "Regenerate SSH keys"
rm -f /etc/ssh/*_key
rm -f /etc/ssh/*_key.pub
ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N "" >/dev/null 2>&1
ssh-keygen -t ecdsa -b 521 -f /etc/ssh/ssh_host_ecdsa_key -N "" >/dev/null 2>&1
ssh-keygen -t ed25519 -b 521 -f /etc/ssh/ssh_host_ed25519_key -N "" >/dev/null 2>&1

log_seq "Checking/resetting sshd to enable only public key auth"
sed -i -e 's/permitrootlogin.*/PermitRootLogin no/gI' /etc/ssh/sshd_config
sed -i -e 's/ChallengeResponseAuthentication.*/ChallengeResponseAuthentication no/gI' /etc/ssh/sshd_config
sed -i -e 's/PasswordAuthentication.*/PasswordAuthentication no/gI' /etc/ssh/sshd_config
sed -i -e 's/usePAM.*/usePAM yes/gI' /etc/ssh/sshd_config
#sed -i -e 's/PermitTTY.*/PermitTTY no/gI' /etc/ssh/sshd_config
sed -i -e 's/AuthenticationMethods.*/AuthenticationMethods publickey/gI' /etc/ssh/sshd_config
sed -i -e 's/HostBasedAuthentication.*/HostBasedAuthentication no/gI' /etc/ssh/sshd_config
