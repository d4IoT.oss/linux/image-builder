#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Security hardening
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done


#########################################################
log_seq "Processing Users in Password File"
declare -i tUid=0
declare -i tGid=0
unset IFS
while IFS=: read tUser tPass tUid tGid tFull tHome tShell
do
    #echo "Processing $tUser $tUid $tGid"
    #Check for non root  uid of 0
    if [[ ("$tUser" != "root") && (($tUid -eq 0) || ($tGid -eq 0)) ]]
	then
       passwd $tUser -l >& /dev/null
       userdel -f $tUser >& /dev/null
       log_warn "User DELETED: rooted $tUser with UID/GID $tUid/$tGid"
    fi
    #Check for password hashes
    if [ "$tPass" != "x" ]
    then
       log_warn "User $tUser has password in password file - Removing"
       sed -i "s/^$tUser:$tPass:/$tUser:x:/" /etc/passwd
    fi
    #Check for accounts to delete
    case $tUser in games|ftp|gopher|halt|reboot|shutdown|news|uucp|operator|netdump|nfsnobody|mailnull)
             passwd $tUser -l >& /dev/null
             userdel -f $tUser >& /dev/null
             log_warn "User DELETED: unwanted $tUser with UID/GID $tUid/$tGid"

            ;;
    esac

    #Check for nologin - all users below 500 and non-root
    case "$tShell" in
	   "/sbin/nologin"|"/usr/bin/nologin"|"/bin/false"|"/bin/true")
	       echo "">/dev/null
	   ;;
	   *)
       if [[ ($tUid -gt 0) && ($tUid -lt 500) ]]
       then
           passwd $tUser -l >& /dev/null
           userdel -f $tUser >& /dev/null
           log_warn "User nologin DELETED: $tUser with UID/GID $tUid/$tGid and shell $tShell"
       else
           if [[ ($tUid -ge 500) && ("$tShell" != '/bin/bash') ]]
           then
               log_warn "User DISABLED: $tUser with UID/GID $tUid/$tGid and shell $tShell - MUST be /bin/bash"
           fi
       fi

    esac
    #Check for Shell

done < /etc/passwd
unset IFS
#########################################################
log_seq "Processing Users in Shadow  File"
while IFS=: read tUser tPass tPwdLastCh tPwdMayCh tPwdMustCh tPwdWarnExp tPwdWarnDisable tWarnDisabled tRes
do
    tUserP=':'
    tTmp=`grep "$tUser:" /etc/passwd`
    IFS=':' read  tUserP tPassP tUid tGid tFull tHome tShell <<< "$tTmp"
    #echo "Processing $tUser $tUserP $tUid $tGid"
    if [ "$tUser" != "$tUserP" ]
    then
        log_error "Could NOT find password record from $tUser"
        continue
    fi
    if [[ ($tUid -gt 0) && ($tUid -lt 500) ]]
    then
        if [ "$tPass" != "*" ]
        then
            log_warn "Priviliged User $tUser Password being removed"
            sed -i "s/^$tUser:$tPass:/$tUser:*:/" /etc/shadow
        fi
    else
        tNumVal=${#tPass}

        if [ $tNumVal -eq 0 ]
        then
            log_warn "Locking passwordless account $tUser"
            sed -i "s/^$tUser::/$tUser:!!:/" /etc/shadow
        fi
    fi
done < /etc/shadow
unset IFS



log_seq "Update PAM files /etc/pam.d"
for tFileSym in system-auth password-auth smartcard-auth fingerprint-auth
do
    tFile0="/etc/pam.d/$tFileSym"
    [ ! -f $tFile0 ] && continue
    tFile1="/etc/pam.d/$tFileSym-LOCAL"
    tFile2=`readlink -f $tFile0`
    if [ ! -L $tFile0 ] && [ "$tFile1" != "$tFile2" ]
    then
        log_warn "Re-pointing authconfig generated files"
        if [ ! -f $tFile1 ]
        then
            log_warn "Moving $tFile2 to $tFile1"
            mv -f $tFile2 $tFile1
        fi
        log_warn "Relocating Symlink $tFile0 to point to $tFile1"
        ln -s -f $tFile1 $tFile0
    fi
    if  grep nullok  $tFile1 >& /etc/null
    then
        log_warn "Removing nullok in $tFile2"
        sed -i "s/nullok//" $tFile1
    fi
    if  grep "password.*pam_cracklib"  $tFile1 >& /etc/null
    then
        if  ! grep "^password.*pam_cracklib.so.*dcredit=-1 ucredit=-1 ocredit=-1 lcredit=-1 difok=4"  $tFile1 >& /etc/null
        then
            log_warn "Correcting pam_cracklib entry in $tFile2"
            sed -i "s/^password.*pam_cracklib.*$/password requisite pam_cracklib.so try_first_pass retry=3 dcredit=-1 ucredit=-1 ocredit=-1 lcredit=-1 difok=4 type=/" $tFile1
        fi
    fi
    tTmp=`grep "^auth.*pam_unix.so"  $tFile1`
    if  [ -n "$tTmp" ]
    then
        if  ! grep "^auth.*pam_faillock"  $tFile1 >& /etc/null
        then
            log_warn "Correcting pam_faillock entry in $tFile2"
            sed -i "s/$tTmp/$tTmp\nauth        [default=die]      pam_faillock.so authfail deny=3 unlock_time=604800 fail_interval=900\nauth        required      pam_faillock.so authsucc deny=3 unlock_time=604800 fail_interval=900\n/" $tFile1
        #TODO: else Check for individual and update
        fi
    fi
    if grep "password.*pam_unix\.so"  $tFile1 >& /etc/null
    then
        if ! grep -E  "password.*pam_unix.*sha512"  $tFile1 >& /etc/null
        then
            log_warn "Setting password hash to SHA512 in $tFile2"
            sed -i "s/sha[0-9]* /sha512 /" $tFile1
        fi
    fi
done

log_seq "Correct file permissions"
setOGPerm "/etc/shadow" "root" "root" "0"
setOGPerm "/etc/gshadow" "root" "root" "0"
setOGPerm "/etc/passwd" "root" "root" "644"
setOGPerm "/etc/group" "root" "root" "644"
tTmp=`readlink -f  /etc/grub2.cfg`
setOGPerm $tTmp "root" "root" "400"



for tLibFile in /usr/lib /usr/lib64 /lib /lib64
do
    for tFile in `find -L $tLibFile -perm /022 \! -type l`
    do
        setOGPerm  "$tFile" "root" "root" ""
        setPermRestrictive "$tFile" "755"
    done
done


for tLibFile in  /bin /usr/bin /usr/local/bin /sbin /usr/sbin /usr/local/sbin
do
    for tFile in `find -L $tLibFile -perm /022 `
    do
        setOGPerm  "$tFile" "root" "root" ""
        setPermRestrictive "$tFile" "755"
    done
done

log_seq "Checking secure login sources"
if grep -v -i console /etc/securetty  >& /dev/null;
then
    log_warn "restricting /etc/securetty to console only"
    echo "console" >  /etc/securetty
fi
tFile1="/etc/libuser.conf"
[ -f $tFile1 ] && matchUpsert $tFile1 crypt_style sha512


log_seq "Setup system parameters in /etc/sysctl.conf"
matchUpsert '/etc/sysctl.conf' 'kernel.randomize_va_space = 2' '^kernel\.randomize_va_space[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'kernel.exec-shield = 2' '^kernel\.exec-shield[ ]*=.*'




[ ! -d /etc/sysconfig ] && echoInfo "No Sysconfig related items"; return 0

tFile1="/etc/sysconfig/init"
log_seq "Correct sysconfig init parameters"
if ! grep "SINGLE.*sulogin$" $tFile1 >& /dev/null
then
	if grep "SINGLE.*" $tFile1 >& /dev/null
	then
		log_warn "Modify single user mode to require authentication"
		sed -i "s/SINGLE.*$/SINGLE=\/sbin\/sulogin/" $tFile1
	else
		log_warn "Setting single user mode to require authentication"
		echo  "SINGLE=/sbin/sulogin" >> $tFile1
	fi
fi


if ! grep "PROMPT.*no$" $tFile1 >& /dev/null
then
	if grep "PROMPT.*" $tFile1 >& /dev/null
	then
		log_warn "Modify Boot prompt to NO"
		sed -i "s/PROMPT.*$/PROMPT=no/" $tFile1
	else
		log_warn "Setting Boot prompt to NO"
		echo "PROMPT=no" >> $tFile1
	fi
fi
