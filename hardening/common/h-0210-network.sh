#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
my_dir="$(dirname "$(readlink -f "$0")")"
. $my_dir/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description network hardening
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done


log_seq "Checking rhosts"
if [ -e /etc/hosts.equiv ];
then
   log_warn "Removing file /etc/hosts.equiv"
   rm -f /etc/hosts.equiv
fi
for tNewVal in `find / -name .rhosts 2>/dev/null`
do
    log_error "DELETE File $tNewVal"
done

log_seq "Setup IPV4"
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.default.send_redirects = 0' '^net\.ipv4\.conf\.default\.send_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.send_redirects = 0' '^net\.ipv4\.conf\.all\.send_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.ip_forward = 0' '^net\.ipv4\.ip_forward[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.accept_source_route = 0' '^net\.ipv4\.conf\.all\.accept_source_route[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.accept_redirects = 0' '^net\.ipv4\.conf\.all\.accept_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.secure_redirects = 0' '^net\.ipv4\.conf\.all\.secure_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.log_martians = 1' '^net\.ipv4\.conf\.all\.log_martians[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.default.accept_source_route = 0' '^net\.ipv4\.conf\.default\.accept_source_route[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.default.accept_redirects = 0' '^net\.ipv4\.conf\.default\.accept_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.default.secure_redirects = 0' '^net\.ipv4\.conf\.default\.secure_redirects[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.icmp_log_ignore_broadcasts = 1' '^net\.ipv4\.icmp_log_ignore_broadcasts[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.icmp_ignore_bogus_error_responses = 1' '^net\.ipv4\.icmp_ignore_bogus_error_responses[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.tcp_syncookies = 1' '^net\.ipv4\.tcp_syncookies[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.all.rp_filter = 1' '^net\.ipv4\.conf\.all\.rp_filter[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv4.conf.default.rp_filter = 1' '^net\.ipv4\.conf\.default\.rp_filter[ ]*=.*'
matchUpsert '/etc/sysctl.conf' 'net.ipv6.conf.default.accept_redirects = 0' '^net\.ipv6\.conf\.default\.accept_redirects[ ]*=.*'
matchUpsert '/etc/modprobe.d/disabled.conf' 'options ipv6 disable=1' 'options[ ]* ipv6[ ]*disable=.*'
## Lets setup iptables to be very conservative
#TODO: Check if all installs look in same directory - RHEL may look in sysconfig
if [ -d /etc/iptables ]; then
cat <<EOF >/etc/iptables/iptables.rules
*filter
:INPUT DROP
:FORWARD DROP
:OUTPUT DROP
-A INPUT -i lo -j ACCEPT
-A INPUT -s 127.0.0.0/8 -j DROP
-A OUTPUT -o lo -j ACCEPT
-A OUTPUT -p tcp -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -p udp -m state --state NEW,ESTABLISHED -j ACCEPT
-A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT
-A INPUT -p tcp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p udp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p icmp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT


COMMIT

EOF

cat <<EOF >/etc/iptables/ip6tables.rules
:INPUT DROP
:FORWARD DROP
:OUTPUT DROP


-A INPUT -s ::1 -d ::1 -j ACCEPT
-A OUTPUT -p tcp -m state --state ESTABLISHED -j ACCEPT
-A OUTPUT -p icmp -m state --state NEW,ESTABLISHED -j ACCEPT
-A INPUT -p tcp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p udp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p icmp -m state --state ESTABLISHED -j ACCEPT
-A INPUT -p tcp --dport 22 -m state --state NEW -j ACCEPT


COMMIT

EOF

else
  log_warn "Iptables $G_ERROR_BYPASSED"
fi
#firewall-cmd --direct --permanent --add-rule ipv4 filter IN_public_allow 0 -m tcp -p tcp -m limit --limit 25/minute --limit-burst 100 -j ACCEPT
