#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
umask 022

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/common/source-this.sh
. $SCRIPTDIR/../common/display-log-funcs.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 -h this help info
      $0 -p <primary raw device> [-r <release version>]
    @Description Generate a local base Centos Image
    @Arg -h Help. If specified, prints help and exits
    @Arg -p <raw Device>  primary Device
    @Arg -r <release version>  optional Release of OS. Defaults to 8
    @Return
      0 - Success
      !0 - failure
    @Example
    @See $0/common/source-this.sh
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#setup defaults
P_DEVICE=""
P_RELVER=8
#validate Options
while getopts p:r:h opt; do
  case $opt in
    h)
      usage
    ;;
    p)
      P_DEVICE=$OPTARG
    ;;
    r)
      P_RELVER=$OPTARG
    ;;
  esac
done
PKGLIST="$SCRIPTDIR/config/package-list-centos-$P_RELVER"

[[ -z "$P_DEVICE" ]] && usage "Primary raw disk MUST be specified" || :

[[ ! -f $PKGLIST ]] && log_error_exit "Unable to locate package list $PKGLIST"|| :

exec > >(tee -ia /tmp/centos-linux-build.log) 2>&1

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/common/source-this.sh
. $SCRIPTDIR/../common/display-log-funcs.sh
log_msg "Staring Centos Image Prep"
yum -y update kernel

$SCRIPTDIR/common/disk-init.sh -p $P_DEVICE


##CAUTION - If the kernel version does not match the version of system grub2 has a problem
##you will see a warning message like
##  Kernel version xxxxxx has no module directory /lib/modules/xxxxxx
## This sometimes happens during initial kernel install and will require a
## subsquent kernel re-install


#log_msg "Some pre-req bind mounts"
mount -o bind /dev $C_LOC_MNT/dev
mount -o bind /dev/pts $C_LOC_MNT/dev/pts
mount -o bind /dev/shm $C_LOC_MNT/dev/shm
mount -o bind /proc $C_LOC_MNT/proc
mount -o bind /sys $C_LOC_MNT/sys


log_msg "Starting package install into $C_LOC_MNT. Running Quiet - Be patient"
yum  --releasever=$P_RELVER --installroot=$C_LOC_MNT  -y install $(grep -v '^#' $PKGLIST|sed 's/#.*$//g')
if ((P_RELVER < 8)); then
  logMsg "Fixing LVM Daemon required for use of lvm during boot"
  sed -i "s/use_lvmetad = 1/use_lvmetad = 0/" $C_LOC_MNT/etc/lvm/lvm.conf
  chroot $C_LOC_MNT rpm -e --allmatches --nodeps kernel
  yum  --releasever=$P_RELVER --installroot=$C_LOC_MNT -q -y install kernel
fi
yum  --installroot=$C_LOC_MNT -q -y clean all
yum  --installroot=$C_LOC_MNT -q -y clean metadata
[ -d $C_LOC_MNT/var/cache/yum ] && rm -rf $C_LOC_MNT/var/cache/yum



#Required to do grub install to appropriate place
export C_DISK_BOOT=$P_DEVICE


$SCRIPTDIR/common/prep-setup.sh

chroot $C_LOC_MNT $C_LOC_CHROOT_TMP/run-setup.sh

# Something new to be done for Centos 7 - unfortunatley will result in
#   a reboot on first invocation. Care should be taken to recognize this
#   on any one time initializations
touch $C_LOC_MNT/.autorelabel

#Apply hardening
chroot $C_LOC_MNT $C_LOC_CHROOT_SCRIPTS/hardening/common/harden.sh

#TODO: Remove when things are fully working
chroot $C_LOC_MNT $C_LOC_CHROOT_TMP/temp-insecure.sh


#Lets for now save it so we can see before reboot
#$SCRIPTDIR/common/cleanup.sh

#yum clean all
#yum clean metadata
#[[ -d /var/cache/yum ]] && rm -rf /var/cache/yum

log_msg "Finished Centos Image Prep"
