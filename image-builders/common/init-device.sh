#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../common/display-log-funcs.sh
function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 -h | -p <primary raw device>
    @Description Initialize device. **WARNING** This action is not recoverable
    @Arg -h Help. If specified, prints help and exits
    @Arg -p <raw Device>  primary Device
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}


#validate Options
while getopts p:h opt; do
  case $opt in
    h)
      usage
    ;;
    p)
      P_DEVICE=$OPTARG
    ;;
  esac
done
set -e
[[ -z "$P_DEVICE" ]] && usage "Device Must be specified" || :

#mkdir -p /media/cdrom
#mount -r -t iso9660 /dev/sr0 /media/cdrom


set -e

[[ ! -b $P_DEVICE ]] && usage "No such device $P_DEVICE " || :

T_SECTOR_COUNT=`fdisk -l $P_DEVICE | grep $P_DEVICE: | awk '{print $7}'`
T_SECTOR_SIZE=`fdisk -l $P_DEVICE | grep 'Sector size ' | awk '{print $7}'`

log_warn "Init $P_DEVICE bs=$T_SECTOR_COUNT count=$T_SECTOR_SIZE"

dd if=/dev/zero of=$P_DEVICE bs=$T_SECTOR_COUNT count=$T_SECTOR_SIZE
sync;sync;
log_info "Please reboot to cleanout any LVM generations"
