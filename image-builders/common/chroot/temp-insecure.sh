#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../../common/display-log-funcs.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Temporary changes to permit debugging
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done


set -e
log_msg "Starting Insecure Setup"
log_warn "Insecure Setup Initiated"
umask 222
t_ssh_file="/etc/ssh/sshd_config"

sed -i -e 's/permitrootlogin.*/PermitRootLogin yes/gI' $t_ssh_file

sed -i -e 's/ChallengeResponseAuthentication.*/ChallengeResponseAuthentication yes/gI' $t_ssh_file

sed -i -e 's/PasswordAuthentication.*/PasswordAuthentication yes/gI' $t_ssh_file

#sed -i -e 's/usePAM.*/usePAM no/gI' $t_ssh_file

sed -i -e 's/PermitTTY.*/PermitTTY yes/gI' $t_ssh_file

sed -i -e 's/AuthenticationMethods.*/AuthenticationMethods publickey password/gI' $t_ssh_file
tmppasswd=$(date +%s | sha256sum | base64 | head -c 32 ; echo)
tmppasswd='Qu1ckF0x!'
log_warn "Saving(/tmp/tmp-passwd) password($tmppasswd) for root"
echo -n "$tmppasswd" > /tmp/tmp-passwd
#echo -n password| passwd --stdin root
#echo -n root:password| chpasswd
tmppasswdenc="$(openssl passwd -6 "$tmppasswd")"
usermod -p "$tmppasswdenc" root
systemctl enable sshd.service
log_msg "Finished Insecure Setup"
