#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../../common/display-log-funcs.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Misc. chroot executions.
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

SCRIPTDIR="$(dirname "$(readlink -f "$0")")"


set -e
log_msg "Starting Misc. Setup"
[ -f /etc/resolv.conf ] && ( rm -f /etc/resolv.conf ; exit 0 )
[ ! -L /etc/resolv.conf ] && ( ln -s /run/systemd/resolve/resolv.conf /etc/resolv.conf; exit 0 )

#ls -la /etc/resol*
[ ! -d /etc/systemd/network ] && mkdir -p /etc/systemd/network
cat <<EOF > /etc/systemd/network/50-wired.network
[Match]
Name=*
[Network]
DHCP=ipv4
LinkLocalAddressing=no
IPv6AcceptRA=no
EOF

echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf

[[ -f usr/lib/systemd/system/systemd-networkd.service ]]  && systemctl enable systemd-networkd.service || :
systemctl enable systemd-resolved.service

cp -f $SCRIPTDIR/cfg/sshd_config /etc/ssh/sshd_config

log_msg "Finished Misc. Setup"
