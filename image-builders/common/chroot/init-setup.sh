#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../../common/display-log-funcs.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Initial chroot execution
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

log_info "Starting Initial Setup"

umask 222
log_msg "Settingg up locale"
ln -sf /usr/share/zoneinfo/US/Eastern /etc/localtime
localedef  -c -i en_US -f UTF-8 en_US.UTF-8
#sed -i "s/use_lvmetad = 1/use_lvmetad = 0/" /etc/lvm/lvm.conf
echo "LANG=en_US.UTF-8" > /etc/locale.conf



log_msg "Setting Grub"
echo hostInitial > /etc/hostname
[ -f /usr/bin/grub-install ]  && grub-install $C_DISK_BOOT
[ -f /usr/sbin/grub2-install ]  && grub2-install $C_DISK_BOOT
T_CONF="/etc/mkinitcpio.conf"
if [ -f $T_CONF ]; then
  sed -i 's/ block / block sd-lvm2 /g' $T_CONF
  sed -i 's/ udev / systemd /g' $T_CONF
  sed -i 's/ block / block lvm2 /g' $T_CONF
  mkinitcpio -p linux
fi
[ -f /usr/bin/grub-install ]  && grub-mkconfig $C_DISK_BOOT -o /boot/grub/grub.cfg
[ -f /usr/sbin/grub2-install ]  && grub2-mkconfig $C_DISK_BOOT -o /boot/grub2/grub.cfg

systemctl enable sshd


passwd -l root


log_msg "Finished Initial Setup"
