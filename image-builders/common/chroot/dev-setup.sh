#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
      $0 [-d <directory>]
    @Description Setup minimal devices (mknod)
    @Arg -h Help. If specified, prints help and exits
    @Arg -d Uses the specified value as base directory - used for chroot setup
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}
#validate Options
while getopts d:h opt; do
  case $opt in
    h)
      usage
    ;;
    d)
      P_REFDIR=$OPTARG
    ;;
    *)
      usage "Invalid option $OPTARG"
    ;;
  esac
done

log_msg "Starting Device(mknod) Setup at $P_REFDIR"
[ ! -d  ${P_REFDIR}/sys ] && mkdir -p -m 555 ${P_REFDIR}/sys
[ ! -d  ${P_REFDIR}/proc ] && mkdir -p -m 555 ${P_REFDIR}/proc
[ ! -d  ${P_REFDIR}/run ] && mkdir -p -m 755 ${P_REFDIR}/run
[ ! -d  ${P_REFDIR}/dev ] && mkdir -p -m 755 ${P_REFDIR}/dev
[ ! -c  ${P_REFDIR}/dev/null ] && mknod -m 666  ${P_REFDIR}/dev/null c 1 3
[ ! -c  ${P_REFDIR}/dev/zero ] && mknod -m 666  ${P_REFDIR}/dev/zero c 1 5
[ ! -c  ${P_REFDIR}/dev/random ] && mknod -m 666  ${P_REFDIR}/dev/random c 1 8
[ ! -c  ${P_REFDIR}/dev/urandom ] && mknod -m 666  ${P_REFDIR}/dev/urandom c 1 9
[ ! -d  ${P_REFDIR}/dev/pts ] && mkdir -m 755  ${P_REFDIR}/dev/pts
[ ! -d  ${P_REFDIR}/dev/shm ] && mkdir -m 1777  ${P_REFDIR}/dev/shm
[ ! -c  ${P_REFDIR}/dev/tty ] && mknod -m 666  ${P_REFDIR}/dev/tty c 5 0
[ ! -c  ${P_REFDIR}/dev/console ] && mknod -m 600  ${P_REFDIR}/dev/console c 5 1
[ ! -c  ${P_REFDIR}/dev/tty0 ] && mknod -m 666  ${P_REFDIR}/dev/tty0 c 4 0
[ ! -c  ${P_REFDIR}/dev/full ] && mknod -m 666  ${P_REFDIR}/dev/full c 1 7
#[ ! -p  ${P_REFDIR}/dev/initctl && ! -L  ${P_REFDIR}/dev/initctl ] && mknod -m 600  ${P_REFDIR}/dev/initctl p
[ ! -c  ${P_REFDIR}/dev/ptmx ] && mknod -m 666  ${P_REFDIR}/dev/ptmx c 5 2
log_msg "Finished Device Setup"
