#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../../common/display-log-funcs.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Prepare for chroot executions.
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

log_msg "Starting systemd-nspawn preperation"

# Setup to execute actions that cannot be run in chroot and
#   needs a 'full' system

# Disable stuff that will be in the way
systemctl disable sshd
mv /etc/fstab /etc/fstab.sav

cat <<"EOF" > /etc/systemd/system/tmpprep.service
[Unit]
Description=Do Tmp Actions

[Service]
Type=oneshot
ExecStart=/var/tmp/chroot/nspawn-exec.sh > /var/tmp/nspawn-exec.log
ExecStartPost=/bin/systemctl poweroff
RemainAfterExit=yes

[Install]
WantedBy=multi-user.target

EOF

cat <<"EOF" > /var/tmp/chroot/nspawn-exec.sh
#!/bin/bash
set +e


# Perform stuff we need to do

[ -f /sbin/fixfiles ] && /sbin/fixfiles -f -F relabel



# now enable everything we disabled before
systemctl enable sshd
mv  /etc/fstab.sav /etc/fstab

systemctl disable tmpprep.service

exit 0
EOF

chmod 0755 /var/tmp/chroot/nspawn-exec.sh
systemctl enable tmpprep.service

log_msg "Finished systemd-nspawn preperation"
