#!/bin/false
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################


#Main directory to work in
# This directory shoudl not have any open files in parent path or you could have 
#   potential issues with bind mounts giving device busy errors during install
export C_LOC=/image-build
#Tmp directory to work in
export C_LOC_TMP=$C_LOC/tmp
#our new root
export C_LOC_MNT=$C_LOC/root
#our new root
export C_DISMNT_CLEANUP=$C_LOC_TMP/dismount_cleanup_lvms.sh

#Location execute stuff from tmp after chroot
export C_LOC_CHROOT_SCRIPTS=/opt/d4IoT/image-builder-scripts
export C_LOC_CHROOT_MNT_SCRIPTS=$C_LOC_MNT$C_LOC_CHROOT_SCRIPTS
export C_LOC_CHROOT_TMP=$C_LOC_CHROOT_SCRIPTS/image-builders/common/chroot
export C_LOC_CHROOT_MNT_TMP=$C_LOC_MNT$C_LOC_CHROOT_TMP
