#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../common/display-log-funcs.sh
. $SCRIPTDIR/source-this.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Final Cleanup. Remove temp files and dismount drives.
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See $0/source-this.sh
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}
#validate Options
while getopts h opt; do usage; exit 0; done



log_msg "Starting final Cleanup"


(rm -rf $C_LOC_MNT/var/tmp/* ; exit 0)
(rm -rf $C_LOC_MNT/tmp/* ; exit 0)

function rmExcParent() {
  [[ "$1" == */. || "$1" == */.. ]] && return 0
  echo "rm $1"
  rm -rf $1 ; return 0
}

find $C_LOC_MNT/var/tmp/.* -type d -prune -exec rmExcParent {} \;
find $C_LOC_MNT/tmp/.* -type d -prune -exec rmExcParent {} \;

sync;sync;sync

#dismount 
umount -AlR $C_LOC_MNT


rm -d $C_LOC_MNT

rm -rf $C_LOC

sync;sync;sync

log_msg "Finished Final Cleanup"
