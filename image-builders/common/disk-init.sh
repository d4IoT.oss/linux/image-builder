#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../common/display-log-funcs.sh
. $SCRIPTDIR/source-this.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 -h
      $0 -p <primary raw device> [-s <secondary | raw device>]
    @Description Initialize a disk with boot and lvm partitions and filesystem
                 Mount disk to 
    @Arg -h Help. If specified, prints help and exits
    @Arg -p <raw Device>  primary Device
    @Return
      0 - Success
      !0 - failure
    @Example
    @See
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts p:h opt; do
  case $opt in
    h)
      usage
      exit 0
    ;;
    p)
      P_DEVICE=$OPTARG
    ;;
  esac
done

[[ -z "$P_DEVICE" ]] && usage "Raw device must be provided" || :
[[ ! -b $P_DEVICE ]] && usage "No such device $P_DEVICE " || :

parted $P_DEVICE print | grep "Partition Table: unknown" | wc -w | { read wc; test $wc -gt 0 && exit 0 || usage  "Device $P_DEVICE must be raw" ;}

log_info "Starting Disk Initialize"
[ "${P_DEVICE#/dev/nvme}" == "$P_DEVICE" ] && T_PARTID='' || T_PARTID='p'
T_SECTOR_COUNT=0
T_SECTOR_SIZE=0
T_SECTOR_COUNT=`fdisk -l $P_DEVICE | grep $P_DEVICE: | awk '{print $7}'`
T_SECTOR_SIZE=`fdisk -l $P_DEVICE | grep 'Sector size ' | awk '{print $7}'`
T_DISK_TOT_SIZE=$((T_SECTOR_COUNT * T_SECTOR_SIZE))
((T_DISK_TOT_SIZE < 6000000000)) && errExit 'Minimum 6GB is required' 
#Caluclate for 1MB of MBR to include GPT/Grub 1
T_CALT_MBR=$((1024 * 1024 / T_SECTOR_SIZE))
T_EXT=$((1024 * 1024 % T_SECTOR_SIZE))
((T_EXT > 0))  && ((T_CALT_MBR+=1)) || echo -n ''

T_BOOT_SECTORS=$((256*1024*1024 / T_SECTOR_SIZE)) # Set boot to 256M
T_SWAP_SECTORS=$((512*1024*1024 / T_SECTOR_SIZE)) # Set swap to 512M
T_SECTOR_BOOT_START=$T_CALT_MBR
T_SECTOR_LVM_START=$((T_SECTOR_BOOT_START + T_BOOT_SECTORS))
T_SECTOR_SWAP_START=$((T_SECTOR_COUNT - T_SWAP_SECTORS))

#boot label
T_LABEL_BOOT=bootfs
#Root Label
T_LABEL_ROOT=rootfs
#group lvm volumes
T_DISK_VG=vgRoot



log_info "Sectors Start Info :: Boot:$T_SECTOR_BOOT_START LVM:$T_SECTOR_LVM_START SWAP:$T_SECTOR_SWAP_START Bytes Per Sector:$T_SECTOR_SIZE"

mkdir -p $C_LOC_MNT
log_info "Start partitioning of disk $P_DEVICE "
parted -s $P_DEVICE mklabel msdos
parted -s $P_DEVICE mkpart primary xfs ${T_SECTOR_BOOT_START}s $((T_SECTOR_LVM_START-1))s
#Swap is calculated from end of disk - so 100%
#Usually begining and end of physical disks are faster
parted -s $P_DEVICE mkpart primary linux-swap ${T_SECTOR_SWAP_START}s 100% 
#Lvm 
parted -s $P_DEVICE mkpart primary ext4 ${T_SECTOR_LVM_START}s $((T_SECTOR_SWAP_START-1))s
parted -s $P_DEVICE set 3 lvm on
#parted -s $P_DEVICE set 1 boot on
#Lets wait fo OS to register partitions. Sometimes it takes a while
log_msg "Waiting for partition 1 and 3 to be ready before proceeding"
while [[ ! -e ${P_DEVICE}${T_PARTID}3 || ! -e ${P_DEVICE}${T_PARTID}1 ]]; do sleep 1; done
mkfs.xfs -f ${P_DEVICE}${T_PARTID}1
xfs_admin -L $T_LABEL_BOOT  ${P_DEVICE}${T_PARTID}1
mkswap -L swapfs ${P_DEVICE}${T_PARTID}2
T_DISK_ROOT=${P_DEVICE}${T_PARTID}3

log_info "prepare file for dismount and cleanup of LVM gens $C_DISMNT_CLEANUP"
cat <<EOF >$C_DISMNT_CLEANUP
lvremove -fqy /dev/$T_DISK_VG/lvRoot
lvremove -fqy /dev/$T_DISK_VG/lvVar
lvremove -fqy /dev/$T_DISK_VG/lvLog
lvremove -fqy /dev/$T_DISK_VG/lvAudit
lvremove -fqy /dev/$T_DISK_VG/lvHome
lvremove -fqy /dev/$T_DISK_VG/lvOpt
lvremove -fqy /dev/$T_DISK_VG/lvVarTmp
lvremove -fqy /dev/$T_DISK_VG/lvTmp
vgremove -fqy $T_DISK_VG
EOF
chmod 0755 $C_DISMNT_CLEANUP

T_UNDO_FILE="/tmp/unset_disk.sh"
log_info "prepare file for disk re-initialize if required $T_UNDO_FILE"
cat <<EOF >$T_UNDO_FILE
umount -AlR $C_LOC_MNT
$C_DISMNT_CLEANUP
parted -s $P_DEVICE rm 3
parted -s $P_DEVICE rm 2
parted -s $P_DEVICE rm 1
$SCRIPTDIR/init-device.sh -p $P_DEVICE
EOF
chmod 0755 $T_UNDO_FILE


log_info "Creating LVM group($T_DISK_VG) and logical volumes"
vgcreate $T_DISK_VG $T_DISK_ROOT
lvcreate -L 1900M -n  lvRoot     /dev/$T_DISK_VG
lvcreate -L 1000M -n  lvVar      /dev/$T_DISK_VG
lvcreate -L 500M  -n  lvLog      /dev/$T_DISK_VG
lvcreate -L 300M  -n  lvAudit    /dev/$T_DISK_VG
lvcreate -L 100M  -n  lvTmp      /dev/$T_DISK_VG
lvcreate -L 100M  -n  lvHome     /dev/$T_DISK_VG
lvcreate -L 500M  -n  lvOpt      /dev/$T_DISK_VG
lvcreate -L 200M  -n  lvVarTmp   /dev/$T_DISK_VG



log_info "Formatting filesystem on LVM Partitions"
mkfs.xfs -f  /dev/$T_DISK_VG/lvRoot
xfs_admin -L $T_LABEL_ROOT   /dev/$T_DISK_VG/lvRoot
mkfs.xfs -f  /dev/$T_DISK_VG/lvVar
mkfs.xfs -f  /dev/$T_DISK_VG/lvLog
mkfs.xfs -f  /dev/$T_DISK_VG/lvAudit
mkfs.xfs -f  /dev/$T_DISK_VG/lvTmp
mkfs.xfs -f  /dev/$T_DISK_VG/lvHome
mkfs.xfs -f  /dev/$T_DISK_VG/lvOpt
mkfs.xfs -f  /dev/$T_DISK_VG/lvVarTmp


log_info "Mounting at $C_LOC_MNT"
mount -t xfs /dev/$T_DISK_VG/lvRoot          $C_LOC_MNT
mkdir -p $C_LOC_MNT/var  $C_LOC_MNT/boot $C_LOC_MNT/home $C_LOC_MNT/opt
mkdir -p -m 1777 $C_LOC_MNT/tmp
mount -t xfs ${P_DEVICE}${T_PARTID}1         $C_LOC_MNT/boot
mount -t xfs /dev/$T_DISK_VG/lvVar           $C_LOC_MNT/var
mount -t xfs /dev/$T_DISK_VG/lvTmp           $C_LOC_MNT/tmp
mkdir -p $C_LOC_MNT/var/log $C_LOC_MNT/var/data $C_LOC_MNT/var/cache 
mkdir -p -m 1777 $C_LOC_MNT/var/tmp
mount -t xfs /dev/$T_DISK_VG/lvLog           $C_LOC_MNT/var/log/
mkdir -p $C_LOC_MNT/var/log/audit
mount -t xfs /dev/$T_DISK_VG/lvAudit         $C_LOC_MNT/var/log/audit/
mount -t xfs /dev/$T_DISK_VG/lvHome          $C_LOC_MNT/home
mount -t xfs /dev/$T_DISK_VG/lvOpt           $C_LOC_MNT/opt
mount -t xfs /dev/$T_DISK_VG/lvVarTmp        $C_LOC_MNT/var/tmp


chmod 1777 /var/tmp
chmod 1777 /tmp

[ ! -d $C_LOC_TMP ] && mkdir -p $C_LOC_TMP

cat <<EOF >$C_LOC_TMP/fstab
/dev/$C_DISK_VG/lvRoot     /                   xfs            defaults                          0 1
LABEL=$T_LABEL_BOOT        /boot               xfs            defaults                          0 2
/dev/$T_DISK_VG/lvTmp      /tmp                xfs            nodev,nosuid,noexec,rw            0 0
/dev/$T_DISK_VG/lvVar      /var                xfs            defaults                          0 0
/dev/$T_DISK_VG/lvOpt      /opt                xfs            defaults                          0 0
/dev/$T_DISK_VG/lvLog      /var/log            xfs            defaults                          0 0
/dev/$T_DISK_VG/lvAudit    /var/log/audit      xfs            defaults                          0 0
/dev/$T_DISK_VG/lvVarTmp   /var/tmp            xfs            nodev,nosuid,noexec,rw            0 0
/dev/$T_DISK_VG/lvHome     /home               xfs            defaults,nodev                    0 0
LABEL=swapfs               swap                swap           defaults                          0 0
tmpfs                      /dev/shm            tmpfs          defaults,nodev,nosuid,noexec      0 0
EOF


# Some need devices in place before initial install
$SCRIPTDIR/chroot/dev-setup.sh -d "$C_LOC_MNT"



log_info "Finished Disk Initialize/Mount"
