#!/bin/bash
###########################################################################
# (c) Copyright 2017-2018,2021 d4Iot.com, WY, US
#     Author: Lloyd Fernandes <lloyd@d4IoT.com>
#
# SPDX-License-Identifier: GPL-3.0-only
#
# You can be released from the above license by purchasing a commercial
#   license.
###########################################################################
# Copyright/License - Refer to NOTICE and LICENSE files at root of project
###########################################################################
set -e
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
. $SCRIPTDIR/../../common/display-log-funcs.sh
. $SCRIPTDIR/source-this.sh

function usage {
  USAGE=$(cat <<-EOF
    @Usage
      $0 [-h]
    @Description Prepare for chroot executions.
    @Arg -h Help. If specified, prints help and exits
    @Return
      0 - Success
      !0 - failure
    @Example
    @See $0/source-this.sh $0/chroot/source-this.sh
    @ToDO
EOF
  )
  log_usage_exit "$USAGE" "$1"
}

#validate Options
while getopts h opt; do usage; exit 0; done

#Prepare files for chroot execution

log_msg "Starting Prepare Setup"

[ -f $C_LOC_TMP/fstab ] && cp -f $C_LOC_TMP/fstab $C_LOC_MNT/etc/fstab || (echo "No FSTAB" ; exit 1)



[ ! -d $C_LOC_CHROOT_MNT_SCRIPTS ] && mkdir -p $C_LOC_CHROOT_MNT_SCRIPTS
SCRIPTDIR="$(dirname "$(readlink -f "$0")")"
cp -R $SCRIPTDIR/../../hardening $C_LOC_CHROOT_MNT_SCRIPTS/
cp -R $SCRIPTDIR/../../image-builders $C_LOC_CHROOT_MNT_SCRIPTS/
cp -R $SCRIPTDIR/../../common $C_LOC_CHROOT_MNT_SCRIPTS/


echo "export C_DISK_BOOT=\"$C_DISK_BOOT\"" >> $C_LOC_CHROOT_MNT_TMP/source-this.sh


#Lets get rid of it here because when we chroot we cannot do it.
#  We need to do this so we can use systemd's reslov
[ -f $C_LOC_MNT/etc/resolv.conf ] && rm -f $C_LOC_MNT/etc/resolv.conf; echo ""


log_msg "Finished Prepare of Setup"
